import boto3

def lambda_handler(event, context):
    s3 = boto3.client('s3')
    response = s3.list_buckets()
    print(response)
    all_buckets = []

    for bucket in response['Buckets']:
        all_buckets.append(bucket["Name"])

    print(all_buckets)

    for bucket in all_buckets:
        response = s3.put_public_access_block(
            Bucket=bucket,
            PublicAccessBlockConfiguration={
                'BlockPublicAcls': True,
                'IgnorePublicAcls': True,
                'BlockPublicPolicy': True,
                'RestrictPublicBuckets': True
            }
        )
        print(response)
